---
layout: handbook-page-toc
title: Marking tickets as spam in Zendesk
category: Zendesk
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

### Marking tickets as spam in Zendesk

Sometimes it can be necessary to mark a ticket as spam in Zendesk. There are two ways to do it:

**Recommended way:** open the ticket and select [Spam macro](https://gitlab.zendesk.com/agent/admin/macros/360015359719) in the list of macros.
It will add a tag and solve the ticket once applied. CSAT survey won't be sent for tickets with such tag.

**Alternative way:** click the arrow in the right part of a ticket and select `Mark as spam`:

  ![Zendesk Mark as Spam](/handbook/support/workflows/assets/zendesk-mark-as-spam.png)

Marking a ticket as spam using this method suspends the end-user, they won't be able to
submit tickets or access our Help Center anymore. That is why it is recommended to use the first method.

For the details about spam in Zendesk see the issue [Managing Spam Tickets](https://gitlab.com/gitlab-com/support/support-team-meta/issues/1775).
