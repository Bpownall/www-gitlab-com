---
layout: handbook-page-toc
title: "GitLab Legal Team READMEs"
description: "Get to know the Legal Team in our individual README pages"
---

## Legal Team READMEs

- [Rob Nalen (Director of Legal Operations & Contracts)](https://about.gitlab.com/handbook/legal/readmes/robnalen.index.html)
