---
layout: handbook-page-toc
title: Corporate Communications Resources and Trainings
---
## Corporate Communications Resources and Trainings
{:.no_toc}

More information about additional trainings are coming soon.

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Social Media Trainings

### Social Media 101 Training + Certification

Anyone can test their knowledge of social media basics or get up to speed with our social media 101 training and certification. The video runs under 22 minutes and the knowledge assessment could be done in as little as 10 minutes. You could be more confident with social media in less than 45 minutes! 

##### 🔗 [Watch the training here. You'll need to be logged into GitLab's Unfiltered YouTube account in order to access the video.](https://youtu.be/c5UcbNYVTu4)
We're aware of a typo in the middle of the session. Instead of "do no" at 4:43 and 5:46, it should be "do not". Kristen says this in the session, but the slide is missing the last letter.

To obtain a certificate, you will need to complete this [Social Media 101 Knowledge Assessment](https://forms.gle/X5toY6A1jhguYyfj8) and earn at least an 80%. Once the quiz has been passed, you will receive an email with your certificate that you can share on your personal LinkedIn or Twitter pages. If you have questions, please reach out to our L&D team at [learning@gitlab.com](mailto:learning@gitlab.com).
<details>
  <summary markdown='span'>
    What is the social media certification?
  </summary>

GitLab’s social media certification will be received by team members who complete the Learning and Development: Let’s Get Social Media Certified training.
Once you complete the Social Media Assessment you can respond to any GitLab mentions which you feel you can provide value to. Once certified you will feel empowered to talk about GitLab and your work on your own social media channels.

</details>

<details>
 <summary markdown='span'>
 What can I expect out of the training?
 </summary>

This training focuses on enabling team members to supplement their channels with content without a formal Employee Advocacy tool to share from. The social media team will provide tips on how to set your profile up for success, how to curate content, and how to engage like an expert. By completing this training, team members will take their previous social 101 to a 202 skill set. You will walk away with a social media certificate, and personal brand confidence.

</details>

<details>
 <summary markdown='span'>
 How can I get social media certified?
 </summary>

It is not required to be active on social media as a GitLab team member. It is also not required that you speak on behalf of GitLab on social platforms. However, if you do talk about work-related matters that are within your area of job responsibility- you must disclose your affiliation with GitLab. And before you do- you must get certified after taking the L&D: Let’s Get Social Media Certified training.

</details>

<details>
  <summary markdown='span'>
    Social Media Knowledge Assessment
  </summary>

Anyone can become certified in the GitLab Social Media Training. To obtain certification, you will need to complete this Social Media Knowledge Assessment and earn at least an 80%. Once the quiz has been passed, you will receive an email with your certification that you can share on your personal LinkedIn or Twitter pages. If you have questions, please reach out to our L&D team at learning@gitlab.com.

</details>

### GitLab Social Media Resources

_Team members should familiarize themselves with personal brand tips and the GitLab Branded Social Media Channels._

- [Social Media Policy and Guidelines](https://about.gitlab.com/handbook/marketing/social-media-guidelines/)
- [Social Marketing Handbook](https://about.gitlab.com/handbook/marketing/corporate-marketing/social-marketing/)
- [GitLab Branded Social Channels](https://about.gitlab.com/handbook/marketing/corporate-marketing/social-marketing/#primary-social-channels-audiences-and-calendaring-)
- [Contribute 2020: How to be a Social Media Contributor](https://www.youtube.com/watch?v=csWMZuUM3w8#action=share)

### General Social Media Resources

_Here are additional resources on how to build your personal brand on social media._

- [10 Golden Rules of Personal Branding](https://www.forbes.com/sites/goldiechan/2018/11/08/10-golden-rules-personal-branding/#4fd8637358a7)
- [Personal Branding Trends to Tap Into](https://sproutsocial.com/insights/personal-branding-trends-social-media/)
- [The Ultimate Guide to Personal Branding](https://sproutsocial.com/insights/personal-branding/)
