- name: Enablement - Section PPI, Stage PPI - Median End User Latency
  base_path: "/handbook/product/performance-indicators/"
  definition: Median end user latency collected by Real User Monitoring. This metric
    captures how well we are doing in serving our primary customers, our end users,
    by measuring their end user experience.
  target: TBD
  org: Enablement Section
  section: enablement
  stage: enablement
  public: true
  pi_type: Section PPI, Stage PPI
  product_analytics_type: Both
  is_primary: true
  is_key: false
  health:
    level: 2
    reasons:
      - We know we are [slower than our primary competition](https://dashboards.gitlab.net/d/performance-comparison/github-gitlab-performance?orgId=1)
  implementation:
    status: Instrumentation
    reasons:
    - Working on [implementing RUM on GitLab.com](https://gitlab.com/gitlab-org/gitlab/-/issues/218507), currently in progress for 13.5.
    - No target defined, as metric is not instrumented yet
    - Data engineering is working to [enable snowflake dbt for snowplow performanceTiming metrics](https://gitlab.com/gitlab-data/analytics/-/issues/6632)
  lessons:
    learned:
    - GitLab.com free usage has [returned to positive growth](https://app.periscopedata.com/app/gitlab/738689/Reduction-in-Free-usage) after the summer lull
    - Self-managed usage [continues to plateau](https://app.periscopedata.com/app/gitlab/738689/Reduction-in-Free-usage)
    - GitLab.com is [accelerating faster](https://app.periscopedata.com/app/gitlab/738689/Reduction-in-Free-usage) than self-managed
    - WIP ["Shift to SaaS" dashboard](https://app.periscopedata.com/app/gitlab/742037/Enablement:-Shift-to-SaaS) to determine whether we are capturing the "lost" growth on .com
    urls:
      - https://gitlab.com/gitlab-data/analytics/-/issues/5657
      - https://gitlab.com/gitlab-data/analytics/-/issues/6632
  metric_name: performanceTiming

- name: Enablement:Distribution - Group PPI - Percentage of installations on the 3 most
    recent versions of GitLab
  base_path: "/handbook/product/performance-indicators/"
  definition: Of the total number of self-managed installs, what percentage are on
    one of the three most recent versions.
  target: 40%
  org: Enablement Section
  section: enablement
  stage: enablement
  group: distribution
  public: true
  pi_type: Group PPI 
  product_analytics_type: Both
  is_primary: true
  is_key: false
  health:
    level: 2
    reasons:
    - A target of 40% has been set. 40% is the upgrade rate that was reached in early 2019.
    - Insights - The upgrade rate increased in from 25% to 28% in September 2020. This is the first increase since April. However, this is still well below the target of 40%. The steady decline in upgrade rate started in May with the release of 13.0 as the latest release. About 30% of active instances are on the minimum required version of PostgreSQL.
    - Improvement - Implementing additional checks such as free space check during a Postgres update to increase success rate. [New chart](https://app.periscopedata.com/app/gitlab/441909/Active-Instances?widget=9900400&udv=1102166) to track PostgreSQL upgrade trends.
  implementation:
    status: Complete
    reasons:
    - Primary PPI is complete, and target is set.
    - Instrumentation of some sub metrics is still in progress
  metric_name: versions_behind_latest
  sisense_data:
    chart: 8658008
    dashboard: 406972
    embed: v2
  sisense_data_secondary:
    chart: 8890933
    dashboard: 406972
    embed: v2
  urls:
  - https://gitlab.com/gitlab-data/analytics/-/issues/4791
  - https://gitlab.com/gitlab-data/analytics/-/issues/4800
  - https://app.periscopedata.com/app/gitlab/406972/Version-Upgrade-Rate?widget=5697571&udv=832292

- name: Enablement:Geo - Paid GMAU - Number of unique users utilizing a Geo secondary
  base_path: "/handbook/product/performance-indicators/"
  definition: Number of unique users utilizing a Geo secondary. This adoption metric
    helps us understand whether end users are actually seeing value in, and are using,
    geo secondaries.
  target: TBD
  org: Enablement Section
  section: enablement
  stage: enablement
  group: geo
  public: true
  pi_type: Paid GMAU
  product_analytics_type: Both
  is_primary: true
  is_key: false
  health:
    level: 0
    reasons:
    - No target defined yet, as we don't have instrumentation
    - Insight -  The number of [Geo nodes deployed](https://app.periscopedata.com/app/gitlab/500159/Geo?widget=6472090&udv=0)
      has continued to grow over second half of 2020. We know that Geo has low
      penetration as a percentage of total deployments, but skews heavily toward the
      large enterprise with a [25% percentage of Premium+ user share](https://docs.google.com/presentation/d/1imw_PWKZhJpxRs_VwTa-SWgwbZJhw2jKp6wMRk2Fo3c/edit#slide=id.g807f195cca_0_768).
    - Improvement - We are working to add support for replicating all data types,
      so that Geo is on a solid foundation for both DR and Geo Replication.
  implementation:
    status: Instrumentation
    reasons:
    - Geo is not available on GitLab.com today, so cannot use Snowplow or the .com
      database.
    - Ability for Geo secondaries to [support usage ping](https://gitlab.com/gitlab-org/gitlab/-/issues/231257) is underway in 13.5.
  urls:
  - https://gitlab.com/gitlab-org/gitlab/-/issues/231257
  metric_name: geo_nodes
  sisense_data:
    chart: 10039214
    dashboard: 758607
    embed: v2
  sisense_data_secondary:
    chart: 10039565
    dashboard: 758607
    embed: v2    

- name: Enablement:Memory - Group PPI - Memory Consumed
  base_path: "/handbook/product/performance-indicators/"
  definition: Average memory consumed by all invididual GitLab processes
  target: 1.5GB
  org: Enablement Section
  section: enablement
  stage: enablement
  group: memory
  public: true
  pi_type: Group PPI
  product_analytics_type: Both
  is_primary: true
  is_key: false
  implementation:
    status: Complete
    reasons:
    - Working to better support topology metrics for [multi-node instances](https://gitlab.com/groups/gitlab-org/-/epics/3576) and [Kubernetes](https://gitlab.com/groups/gitlab-org/-/epics/3577)
    - Working to adjust chart to indicate how many of each process are started by default to better represent a default configuration.
  metric_name: topology.nodes[0].node_services
  sisense_data:
      chart: 10026240
      dashboard: 679200
      embed: v2
  health:
    level: 2
    reasons:
    - We are targeting to get below 2GB of total RAM usage, so we need to get these services [further below](https://gitlab.com/groups/gitlab-org/-/epics/448) that target.

- name: Enablement:Global Search - Paid GMAU - The number of unique paid users per month
  base_path: "/handbook/product/performance-indicators/"
  definition: The number of unique active users and unique paid users interacting with either Basic Search or Advanced Search per month.
  target: 10% month over month (SaaS and self-managed combined)
  org: Enablement Section
  section: enablement
  stage: enablement
  group: global_search
  public: true
  pi_type: Paid GMAU
  product_analytics_type: Both
  is_primary: true
  is_key: false
  health:
    level: 3
    reasons:
    - Heavy growth is expected for Self-managed as we see customers ramp up migrating to verson 13.4+
    - Growth is expected in SaaS as we have been adding new customers and features
  implementation:
    status: Complete
    reasons:
    - Some data is not showing for SaaS Paid GMAU due to a [label change](https://gitlab.com/gitlab-data/analytics/-/issues/6683#note_437464387). SaaS GMAU started in September.
    - Self-managed data was implemented in 13.4.
  sisense_data:
    chart: 10039566
    dashboard: 758607
    embed: v2
  sisense_data_secondary:
    chart: 10039216
    dashboard: 758607
    embed: v2

- name: Enablement:Database - Group PPI - Database Query Apdex (100ms target, 250ms tolerable)
  base_path: "/handbook/product/performance-indicators/"
  definition: Database query Apdex, with 100ms the target and 250ms tolerable. This
    measures the ratio of queries which complete within the satisfactory time, informing
    how well the database is scaling and performing.
  target: 0.99
  org: Enablement Section
  section: enablement
  stage: enablement
  group: database
  public: true
  pi_type: Group PPI
  product_analytics_type: Both
  is_primary: true
  is_key: false
  health:
    level: 3
    reasons:
    - Improvement - We are focusing on [partitioning](https://gitlab.com/groups/gitlab-org/-/epics/2023) 
      the largest tables to improveme the performance and scalability of the database.
  implementation:
    status: Complete
    reasons:
    - The [PPI has been instrumented](https://gitlab.com/gitlab-org/gitlab/-/issues/227305) in 13.4, but data is just starting to come back.
  metric_name: query_apdex_weekly_average
  sisense_data:
    chart: 9885641
    dashboard: 754160
    embed: v2
  sisense_data_secondary:
    chart: 10091150
    dashboard: 754160
    embed: v2
  urls:
  - https://gitlab.com/gitlab-org/gitlab/-/issues/227305

- name: Enablement:Infrastructure - Paid GMAU - Number of unique paid users that perform
    an action on GitLab.com
  base_path: "/handbook/product/performance-indicators/"
  definition: The sum of all monthly active users on gitlab.com that roll up to paid
    accounts in a 28 day rolling period.
  target: 111750
  org: Enablement Section
  section: enablement
  stage: enablement
  group: infrastructure
  public: true
  pi_type: Paid GMAU
  product_analytics_type: SaaS
  is_primary: true
  is_key: false
  health:
    level: 3
    reasons:
    - Met Q3 Target
  implementation:
    status: Complete
    reasons:
    - Instrumentation complete.
  metric_name: COUNT_EVENTS_ACTIVE_USERS_LAST_28_DAYS_BY_PLAN_WAS_PAID
  sisense_data:
    chart: 9655306
    dashboard: 710777
    embed: v2
