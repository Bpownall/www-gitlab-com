---
layout: markdown_page
title: Product Direction - Product Analytics
description: "Product Analytics manages a variety of technologies that are important for GitLab's understanding of how our users use our products. Learn more here!"
canonical_path: "/direction/product-analytics/"
---

## On this page
{:.no_toc}

- TOC
{:toc}

## Vision

The vision of Product Analytics is to help us build a better Gitlab. We collect data about how GitLab is used to understand what product features to build, to understand how to serve our customers, and to help our customers understand their DevOps lifecycle.

## Roadmap

### Privacy Policy and Settings - Rollout Updated Privacy Policy

Teams Involved: Legal, Product Analytics, Data, Security

**Why This Is Important:**

- Our [privacy policy](https://about.gitlab.com/privacy/) has not been updated since the [October 29, 2019 Update on free software and telemetry](https://about.gitlab.com/blog/2019/10/10/update-free-software-and-telemetry/). This policy needs to be updated to align with our Product Analytics direction.
- Privacy settings in GitLab are currently scattered in several places.

**Recent Progress:**

- Draft privacy policy is in place
- We're working on resourcing to build out the Privacy Control Center

**What's Next:**

- [Privacy Policy](https://gitlab.com/groups/gitlab-com/-/epics/907) - work through draft privacy policies and product analytics policies, gather internal and community feedback, create a communications and rollout plan.
- Privacy Control Center - we're building a centralized privacy settings page in GitLab for users to easily understand and control their privacy settings.

### Data Collection - Maintain and Scale Usage Ping

Product Analytics Steps: Collection Framework, Event Dictionary, Instrumentation, Usage Ping Generation, Usage Ping Collector	

Teams Involved: Product Analytics, Data, Product Managers

**Why This Is Important:**

- Product Metrics are currently instrumented by all product teams.
- We're close to 3x the number of metrics tracked since the beginning of this year, as such, we need to ensure metrics are added in a structured way.
- Usage Ping on GitLab.com takes over 32 hours to generate, this is 4x longer than at the beginning of this year.

**Recent Progress:**

- Over 800 metrics tracked in Usage Ping
- Product Analytics review process
- Usage Ping and Snowplow documentation
- Client and server-side counts can now be tracked on SaaS and Self-Managed.
- Client and server-side events are a work in progress on Self-Managed.
- Plan-level reporting using SaaS Usage Ping is not possible as SaaS is multi-tenant and Usage Ping reports at an Instance level.
- We've implemented an [Event Dictionary for Usage Ping](https://gitlab.com/groups/gitlab-org/-/epics/4174)

**What's Next:**

- Usage Ping Standardizations and Data Integrity - create a standardized naming convention, define usage ping metrics in a centralized YAML file, generate the event dictionary automatically from this file.
- Usage Ping Parallelization - compute usage ping metrics in parallel instead of serially. 
- Usage Ping Collector - build out a versioned Usage Ping collector endpoint to collect restructured Usage Ping data.


### Data Collection - Enable Snowplow Event Tracking

Product Analytics Steps: Collection Framework, Event Dictionary, Instrumentation, Product Usage, Snowplow Collector, Snowplow Enricher

Teams Involved: Product Analytics, Data, Infrastructure, Product Managers

**Why This Is Important:**

- WIP

**Recent Progress:**

- Proof on concept for Product Analyitcs Snowplow and Usage Ping built.

**What's Next:**

- Snowplow Tooling, Standardizations, and Linking with Usage Ping - Standardize Snowplow schemas and instrumentation methods, create an Event Dictioanry for Snowplow, build out development and testing tools along with documentation. Finish tying Usage Ping and Product Analytic's Snowplow together specifically merging tracking classes into a single class with multiple destinations.
- Scale Product Analytics - Take ownership of our Snowplow Infrastructure and maintain the snowplow collector and enricher. Scale Product Analytics's internal database and collector while working towards supporting an external database and collector


### Processing Pipeline - Decrease Cycle Times for Product Analytics

Product Analytics Steps: Release Cycle, Product Usage, Usage Ping Generation, Usage Ping Collector, Extractors, Loaders, Snowflake Data Warehouse


Teams Involved: Product Analytics, Data, Infrastructure, Product Managers

**Why This Is Important:**

- Data Availability cycle times are currently a 51 day cycle
- Exports of the Versions DB are currently done manually every 14 days according to [this schedule](https://gitlab.com/groups/gitlab-data/-/epics/162).

**Recent Progress:**

- Proof of concept built for exporting data from Versions application using CI pipelines.

**What's Next:**

- [Automate Versions DB Exports](https://gitlab.com/gitlab-org/product-analytics/-/issues/398) - currently Usage Pings are stored in the Versions DB and this is manually exported into the data warehouse every 14 days. This is a manual process by both the infrastructure and data engineering teams. This will allow us reduce import times from 14 days to 1 day.	
- Generate Usage Ping Daily - currently Usage Pings are generated every 7 days, this change will make generating and sending Usage Ping every day. This allows us to reduce generation times from 7 days to 1 day.	

### Processing Pipeline - Plan and Group-level Reporting for SaaS

Product Analytics Steps: Collection Framework, Usage Ping Generation, Collectors, Processors, Snowflake Data Warehouse, dbt Data Model

Teams Involved: Product Analytics, Data 

**Why This Is Important:**

- Currently Usage Ping is not segmented by plan type which means for any SaaS free / paid segmentation cannot be done using Usage Ping. Instead, as a work around, the data team is using the Postgres database import and manually recreating all Usage Ping queries in Sisense.
- A single usage ping on SaaS takes 32+ hours to generate, for group level metrics, we need to run this 1 million times which is only feasible if it's done in the data warehouse.


**Recent Progress:**

- [Usage Ping SQL queries exported](https://gitlab.com/gitlab-org/product-analytics/-/issues/423)

**What's Next:**

- Export Usage Ping Queries - export SQL queries for all Postgres based metrics, find a way to export non-SQL queries for Usage Ping metrics in Redis, integrations, settings.
- Data Modelling and Scheduling - setup the Snowflake EDW to run Usage Ping query workload, build dbt Base Data Models and Product Data Models, setup transformation pipeline schedule.


### Product Performance Indicators

Product Analytics Steps: Instrumentation, Sisense Dashboard, Performance Indicators, Metrics Reviews, Product Improvements

Teams Involved: Product Managers, Product Analytics, Data

**Why This Is Important:**

- Product performance indicators help guide each of our product sections, stages, and groups to be metrics driven.

**Recent Progress:**

- FY21-KR1 Delivered: (EVP, Product) [100% of groups have Future GMAU (or Paid GMAU) with instrumentation, dashboards, and quarterly targets](https://gitlab.com/gitlab-com/Product/-/issues/1342)
- FY21-KR2 Delivered: (EVP, Product) [100% of stages have Future SMAU and Paid SMAU with instrumentation, dashboards, and quarterly targets](https://gitlab.com/gitlab-com/Product/-/issues/1343) 
- All [stage and group performance indicators](https://about.gitlab.com/handbook/product/stage-and-group-performance-indicators/) have been added to the handbook. 
- Process defined for [implementing product performance indicators](https://about.gitlab.com/handbook/product/product-analytics-guide/#implementing-product-performance-indicators)

**What's Next:**

- Guide instrumentation of remaining PIs - work with [stages and groups](https://about.gitlab.com/handbook/product/stage-and-group-performance-indicators/) to implement their metrics
- Intersect and union across PIs - allow the ability to combine multiple metrics and PIs together.
- Define metrics review process - align sections on monthly review process.
- Improve data quality of PIs - our first pass at performance indicators needs to be continuously alongside of our collection framework and data infrastructure.

### Project Compass

Product Analytics Steps: dbt Base Data Models, Product Data Models, Customer Success Data Models, Enterprise Dimensionsal Models, Snowflake EDW to Salesforce Data Pump, Salesforce Dashboards, Salesforce to Gainsight Data Feed, Gainsight Dashboards, Customer Conversations

Teams Involved: Sales, Customer Success, Product Analytics, Data

**Why This Is Important:**

- Project Compass is important as it increases our sales & marketing efficiencies.

**Recent Progress:**

- [Versions App Reporting has been replicated in Sisense Dashboards](https://gitlab.com/gitlab-data/analytics/-/issues/4488/)
- A datafeed has been setup from Sisense to Salesforce
- License data has been added to this data feed

**What's Next:**

- Customer Success Data Models - create SCM, CI, and DevSecOps use cases.
- Tie Product Usage Data to Fulfillment Data - tie together our Usage Pings with license and customer information.
- SaaS Group/Account Level Product Usage + SaaS Product Data Models	- feed SaaS account level data into Salesforce and Gainsight

## Quick Links

| Resource                                                                                                                          | Description                                               |
|-----------------------------------------------------------------------------------------------------------------------------------|-----------------------------------------------------------|
| [Product Analytics Guide](/handbook/product/product-analytics-guide)                                                                              | A guide to Product Analytics                   |
| [Usage Ping Guide](https://docs.gitlab.com/ee/development/product_analytics/usage_ping.html)                                              | An implementation guide for Usage Ping                    |
| [Snowplow Guide](https://docs.gitlab.com/ee/development/product_analytics/snowplow.html)                                                  | An implementation guide for Snowplow                      |
| [Event Dictionary](/handbook/product/product-analytics-guide#event-dictionary)                                        | A SSoT for all collected metrics and events               |
| [Privacy Policy](/privacy/)                                                                                                       | Our privacy policy outlining what data we collect and how we handle it     |
| [Implementing Product Performance Indicators](/handbook/product/product-analytics-guide#implementing-product-performance-indicators)                                   | The workflow for putting product performance indicators in place   |
| [Product Analytics Direction](/direction/product-analytics/)                                                                              | The roadmap for Product Analytics at GitLab                       |
| [Product Analytics Development Process](/handbook/engineering/development/growth/conversion-product-analytics/) | The development process for the Product Analytics groups         |
